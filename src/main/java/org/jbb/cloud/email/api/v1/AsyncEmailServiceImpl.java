package org.jbb.cloud.email.api.v1;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class AsyncEmailServiceImpl implements AsyncEmailService {

  private final RabbitTemplate rabbitTemplate;

  @Override
  public void asyncSendEmail(Email email) {
    rabbitTemplate.convertAndSend(EmailConstraints.EMAIL_DELIVERY_QUEUE_NAME, email);
  }
}
