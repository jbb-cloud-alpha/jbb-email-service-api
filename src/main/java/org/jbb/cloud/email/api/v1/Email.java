package org.jbb.cloud.email.api.v1;

import java.io.Serializable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Email implements Serializable {

  @org.hibernate.validator.constraints.Email
  private String recipientEmail;
  private String topic;
  private String content;

}