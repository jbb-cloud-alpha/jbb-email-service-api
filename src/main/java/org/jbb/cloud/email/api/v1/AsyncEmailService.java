package org.jbb.cloud.email.api.v1;

public interface AsyncEmailService {

  void asyncSendEmail(Email email);

}
