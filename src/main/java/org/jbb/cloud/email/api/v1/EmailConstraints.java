package org.jbb.cloud.email.api.v1;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EmailConstraints {

  public static final String EMAIL_DELIVERY_QUEUE_NAME = "queue.jbb-email-service.to-delivery";

}
