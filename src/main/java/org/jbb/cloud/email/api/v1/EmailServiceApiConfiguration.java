package org.jbb.cloud.email.api.v1;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class EmailServiceApiConfiguration {

  @Bean
  public Queue emailDeliveryQueue() {
    return new Queue(EmailConstraints.EMAIL_DELIVERY_QUEUE_NAME);
  }

}
